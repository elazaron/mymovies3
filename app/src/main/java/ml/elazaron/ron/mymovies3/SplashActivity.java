package ml.elazaron.ron.mymovies3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
       // TextView loadingTV = (TextView) findViewById(R.id.editTextLoading);
       // Animation animation;
        //animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
        //loadingTV.startAnimation(animation);

        Thread timer = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{

                    Intent openMainActivity= new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(openMainActivity);
                }
            }
        };
        timer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
