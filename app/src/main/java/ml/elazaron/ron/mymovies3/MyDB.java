package ml.elazaron.ron.mymovies3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class MyDB  extends SQLiteOpenHelper {
    public static final String dbName = "movies.db";
    public static final String tableName = "movies";
    public static final String movieTitle = "title";
    public static final String movieSynopsis = "synopsis";
    public static final String moviePic = "img_url";
    public static final String movieRating = "rating";
    public static final String movieIsWatched = "is_seen";

    private static MyDB dbInstance = null;

    private MyDB(Context context) {

        super(context,dbName , null, 1);
    }

    public static MyDB getInstance(Context ctx) {

        if (dbInstance == null) {
            dbInstance = new MyDB(ctx.getApplicationContext());
        }
        return dbInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String command="CREATE TABLE "+ tableName+" ( _id  INTEGER PRIMARY KEY AUTOINCREMENT, " +
                " "+ movieTitle +" TEXT, " +
                " "+ movieSynopsis +" TEXT, " +
                " "+ moviePic+ " TEXT, "+
                " " + movieRating  +" REAL, "+
                " " + movieIsWatched  +" INTEGER   )";
        db.execSQL(command);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public Cursor getAll(){
        return getReadableDatabase().query(MyDB.tableName, null,null,null,null,null,null);
    }

    public Cursor getRow(int movie_id){
        return getReadableDatabase().query(MyDB.tableName,null,"_id=?",new String[]{""+movie_id},null,null,null);
    }

    public void deleteAll(){
        getWritableDatabase().execSQL("delete from "+MyDB.tableName);

    }
    public void deleteRow(long id){
        getWritableDatabase().delete(MyDB.tableName,"_id=?",new String[]{String.valueOf(id)});
    }
    public void insert(ContentValues cv){
        getWritableDatabase().insert(MyDB.tableName, null, cv);
    }
    public void update(int movie_id,ContentValues cv){
        getWritableDatabase().update(MyDB.tableName, cv,"_id=?", new String[]{""+movie_id} );
    }
}
