package ml.elazaron.ron.mymovies3;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FindOnlineActivity extends AppCompatActivity implements View.OnClickListener{

    Button buttonSearch;
    Button buttonCancel;
    OkHttpClient client;
    private Handler handler;
    List<String> titles;
    List<String> titlesIDs;
    ListView lv;
    ArrayAdapter<String> adapter;
    EditText searchView;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_online);
        titles = new ArrayList<>();
        titlesIDs = new ArrayList<>();
        handler = new Handler();
        client = new OkHttpClient();

        buttonSearch = (Button) findViewById(R.id.buttonSearch);
        buttonCancel = (Button) findViewById(R.id.buttonCancelWeb);
        buttonSearch.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);

        lv = (ListView) findViewById(R.id.titlesLV);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, titles);
        lv.setAdapter(adapter);
        searchView = (EditText) findViewById(R.id.movieSearchBox);

searchView.addTextChangedListener(new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.length()>2) {
            searchApi();
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
});



        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                progressDialog = ProgressDialog.show(FindOnlineActivity.this, "Loading:", "Loading please wait...", true);
                progressDialog.setCancelable(true);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setMessage("Loading...");
                //progressDialog.show();
                String myUrl = "http://www.omdbapi.com/?i=" + titlesIDs.get(position);
                Request request = new Request.Builder()
                        .url(myUrl)
                        .build();

                Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        progressDialog.dismiss();

                        parseJsonMovie(response);
                    }
                });

            }
        });

    }

    private void parseJsonMovie(Response response) throws IOException {
        if (response.isSuccessful()) {

            String resp = response.body().string();
            JSONObject topObject = null;
            try {
                topObject = new JSONObject(resp);
                final String title = topObject.getString("Title");
                final String plot = topObject.getString("Plot");
                final String poster = topObject.getString("Poster");
                final String ratingStr = topObject.getString("imdbRating");
                final Float rating ;
                if (!ratingStr.contains("N/A")) {
                    rating = Float.parseFloat(ratingStr) / 2f;
                }else{
                    rating =0f;
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent = new Intent(FindOnlineActivity.this, EditActivity.class);
                        intent.putExtra(Extras.MOVIE_TITLE, title);
                        intent.putExtra(Extras.MOVIE_PLOT, plot);
                        intent.putExtra(Extras.MOVIE_POSTER, poster);
                        intent.putExtra(Extras.MOVIE_RATING, rating);
                        intent.putExtra(Extras.IS_FROM_WEB, 0);

                        ActivityOptionsCompat options = null;

                        if(lv!= null){
                            options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                    FindOnlineActivity.this,lv,getString(R.string.trans_from_web));
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            Bundle bundle = options != null ? options.toBundle() : null;
                            startActivity(intent, bundle);

                        }else{
                            startActivity(intent);
                        }

                    }
                });


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    // generic error dialog with no actual user choice
    private void makeErrorDialog(String errorString) {
        new AlertDialog.Builder(FindOnlineActivity.this)
                .setTitle("Error")
                .setMessage(errorString)
                .setCancelable(true)
                .setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        searchView.requestFocus();
                    }
                }).create().show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.buttonSearch) {
            searchApi();
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
        }
        else if(v.getId()==R.id.buttonCancel){
            finish();
        }
    }

    private void searchApi() {
        progressDialog = ProgressDialog.show(FindOnlineActivity.this, "Loading:", "Loading please wait...", true);
        progressDialog.setCancelable(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading...");

        String searchString = searchView.getText().toString().trim();
        try {
            String query = URLEncoder.encode(searchString, "utf-8");
            Request request = new Request.Builder().url("http://www.omdbapi.com/?s=" + query).build();
            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {}
                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    if (response.isSuccessful()) {
                        View view = getCurrentFocus();
                        if (view != null) {
//                            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        final String resp = response.body().string();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                parseListJson(resp);
                                progressDialog.dismiss();
                            }
                        });
                    }

                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();}
        progressDialog.dismiss();
    }

    private void parseListJson(String resp) {
        try {

            JSONObject topObject = new JSONObject(resp);
            String responseIndicator = topObject.getString("Response");
            if(responseIndicator.contains("False")){
                Toast.makeText(this,topObject.getString("Error"),Toast.LENGTH_SHORT).show();
                //makeErrorDialog(topObject.getString("Error"));
                searchView.requestFocus();
            }
            else {
                JSONArray searchArr = topObject.getJSONArray("Search");
                titles.clear();
                titlesIDs.clear();
                for (int i = 0; i < searchArr.length(); i++) {
                    JSONObject jsonObject = (JSONObject) searchArr.get(i);
                    titles.add(jsonObject.getString("Title"));
                    titlesIDs.add(jsonObject.getString("imdbID"));
                }
                adapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            makeErrorDialog("Unknown error");
        }
    }
}
