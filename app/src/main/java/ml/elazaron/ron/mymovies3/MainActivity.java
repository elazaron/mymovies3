package ml.elazaron.ron.mymovies3;


import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeIntents;

public class MainActivity extends AppCompatActivity {
    Handler handler;
    Cursor cursor;
    MyDB helper;
    NyCursorAdapter adapter;
    ListView lv;
    FloatingActionButton buttonPlus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new Handler();
        helper = MyDB.getInstance(getApplicationContext());
        lv = (ListView) findViewById(R.id.moviesLV);
        buttonPlus = (FloatingActionButton) findViewById(R.id.myFAB);
        buttonPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addmovieDialog();
                }
            });

        refreshList();
    }

    // dialog for Add(+) button to decide if to get the info from internet db or manually by the user
    private void addmovieDialog() {
        LayoutInflater inflater = getLayoutInflater();
        // View view=inflater.inflate(R.layout.titlebar, null);
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(Html.fromHtml("<u>Add Movie</u>"))
                //.setCustomTitle(view)
                .setIcon(android.R.drawable.ic_menu_add)
                .setMessage("Would you like to add a movie from internet db or manually ? ")
                .setCancelable(true)
                .setPositiveButton("Manually", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(MainActivity.this, EditActivity.class));
                    }
                })
                .setNegativeButton("from internet", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(MainActivity.this, FindOnlineActivity.class);
                        //intent.putExtra(Extras.movieID, id);
                        startActivityForResult(intent, Extras.resultOnline);
                    }
                })
                .create().show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    protected void onPause() {

        super.onPause();
        refreshList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.mymenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_exit) {
            finish();
        }
        if (item.getItemId() == R.id.menu_delete_all) {
            helper.deleteAll();
            refreshList();
        }


        return super.onOptionsItemSelected(item);
    }


    // refreshes the list from db
    public void refreshList() {
        cursor = helper.getAll();
        adapter = new NyCursorAdapter(this, cursor);

        if (lv != null) {
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    if (cursor.moveToPosition(position)) {
                        int DBid = cursor.getInt(cursor.getColumnIndex("_id"));
                        ImageView ivThumb = (ImageView) view.findViewById(R.id.movieThumbIV);
                        navigateToEditActivity(DBid, ivThumb);

                    }
                }
            });
            lv.setAdapter(adapter);
        }
        registerForContextMenu(lv);


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
        menu.setHeaderTitle(Html.fromHtml("<u>Item options:</u>"));
        menu.setHeaderIcon(android.R.drawable.ic_menu_edit);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            // navigate to Edit screen
            case R.id.edit:
                ImageView im = (ImageView) info.targetView.findViewById(R.id.movieThumbIV);
                navigateToEditActivity(((int) info.id), im);
                return true;
            // delete movie
            case R.id.delete:
                deleteMovie(((int) info.id));
                return true;
            // share the plot intent to whatever can handle text
            case R.id.share:

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                TextView plotTV = (TextView) info.targetView.findViewById(R.id.movieSyn);
                String tempPlot = plotTV.getText().toString();
                sendIntent.putExtra(Intent.EXTRA_TEXT, tempPlot);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                return true;
            // gets movie title  and sends it to a youtube app if installed
            //  and if not than to a google search on a browser
            case R.id.watch_trailer:
                TextView titleTV = (TextView) info.targetView.findViewById(R.id.movieTitleTV);
                String query = titleTV.getText().toString() +" trailer";
                Intent intent;
                if (YouTubeIntents.canResolveSearchIntent(this)) {
                    intent = YouTubeIntents.createSearchIntent(this, query);
                } else {
                    Uri uri = Uri.parse("http://www.google.com/search?q="+query);
                    intent = new Intent(Intent.ACTION_VIEW, uri);

                }
                startActivity(intent);
                break;

            default:
                return super.onContextItemSelected(item);


        }
        return true;
    }



    private void deleteMovie(long id) {
        helper.deleteRow(id);
        refreshList();

    }

// navigates to Edit screen with shared element transition animation
    private void navigateToEditActivity(int id, ImageView ivThumb) {

        Intent intent = new Intent(MainActivity.this, EditActivity.class);
        ActivityOptionsCompat options =
                ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, ivThumb, getString(R.string.trans_from_main));

        intent.putExtra(Extras.movieID, id);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {

        exitDialog();
    }

    private void exitDialog() {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Exit")
                .setMessage("would you like to exit app ? ")
                .setCancelable(true)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create().show();
    }


}
//    public void fillDummyData() {
//
//        ContentValues cv = new ContentValues();
//        cv.put(MyDB.movieTitle, "title");
//        cv.put(MyDB.movieSynopsis, "synopsis...");
//        cv.put(MyDB.moviePic, "http://ia.media-imdb.com/images/M/MV5BMTQ1MjQwMTE5OF5BMl5BanBnXkFtZTgwNjk3MTcyMDE@._V1_SX300.jpg");
//        cv.put(MyDB.movieRating, 7.6);
//        helper.insert(cv);
//    }
//    private void useTrailerApi(String shortName) {
//        OkHttpClient client = new OkHttpClient();
//        String temp = "http://trailersapi.com/trailers.json?movie=" + Uri.encode(shortName);
//        Request request = new Request.Builder()
//                .url(temp)
//                .build();
//        //http://trailersapi.com/trailers.json?movie=The%20Dark%20Knight%20Rises
//        //http://trailersapi.com/trailers.json?movie=The%20Lord%20of%20the%20Rings%3A%20The%20Fellowship%20of%20the%20Ring
//
//        Call call = client.newCall(request);
//
//        call.enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//
//                if (response.isSuccessful()) {
//
//                    String resp = response.body().string();
//                    JSONArray topArray = null;
//                    try {
//                        topArray = new JSONArray(resp);
//
//                        JSONObject topObj = (JSONObject) topArray.get(0);
//                        String str = topObj.getString("code");
//                        Pattern pattern = Pattern.compile("embed.(.*?)\"");
//                        Matcher m = pattern.matcher(str);
//                        if (m.find()) {
//                            String myString = m.group(1);
//                            Log.wtf("youtube id", myString);
//                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + myString)));
//                        }
//
//
//                        //String[] exprs =  str.
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=cxLG2wtE7TM")));
//                }
//            }
//        });
//    }