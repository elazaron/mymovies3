package ml.elazaron.ron.mymovies3;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Layout;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    EditText titleET;
    EditText synET;
    EditText imgUrlET;
    ImageView posterIV;
    RatingBar ratingBar;
    CheckBox checkBox;
    int movie_id;
    int isWebContent;
    String myBase64Image;
    MyDB helper;
    Handler handler;
    Target target;
    Bitmap tempBitmap;
    boolean convertFlag = false;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_edit_menu, menu);
        menu.setHeaderTitle(Html.fromHtml("<u>Add custom image</u>"));
        menu.setHeaderIcon(android.R.drawable.picture_frame);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case R.id.get_from_camera:
                capturePhoto();
                return true;
            case R.id.get_from_gallery:
                getImageFromGallery();
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }

    private void getImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Extras.PICK_IMAGE_REQUEST );
    }

    @Override
    public void onSaveInstanceState(Bundle b){
       tempBitmap = ((BitmapDrawable)posterIV.getDrawable()).getBitmap();
        b.putParcelable("image", tempBitmap);
    }

    @Override
    public void onRestoreInstanceState(Bundle b){
        try {
            tempBitmap = (Bitmap) b.getParcelable("image");
            posterIV.setImageBitmap(tempBitmap);
            convertFlag = true;
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        handler = new Handler();
        titleET = (EditText) findViewById(R.id.titleET);
        synET = (EditText) findViewById(R.id.synET);
        imgUrlET = (EditText) findViewById(R.id.imgURLET);
        posterIV = (ImageView) findViewById(R.id.picIV);
        ratingBar = (RatingBar) findViewById(R.id.ratingBarEdit);
        checkBox = (CheckBox) findViewById(R.id.checkboxEdit);
        helper = MyDB.getInstance(getApplicationContext());

        Intent intent = getIntent();
        isWebContent = intent.getIntExtra(Extras.IS_FROM_WEB, -1);
        movie_id = intent.getIntExtra(Extras.movieID, -1);
        target = new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                Log.wtf("onBitmapLoaded", "");
                tempBitmap = bitmap;
                myBase64Image = Extras.encodeToBase64(tempBitmap);
                posterIV.setImageBitmap(tempBitmap);
            }
            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                makeDialog("Image url is not valid , try a different url.");
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        };
        if (movie_id != -1) {
            loadDBtoView();
        }
        if (isWebContent != -1) {
            loadIntentToView(intent);
        }
        Button cancelBtn = (Button) findViewById(R.id.buttonCancel);
        if (cancelBtn != null) {
            cancelBtn.setOnClickListener(this);
        }
        Button saveBtn = (Button) findViewById(R.id.buttonOK);
        if (saveBtn != null) {
            saveBtn.setOnClickListener(this);
        }
        Button show = (Button) findViewById(R.id.buttonShow);
        if (show != null) {
            show.setOnClickListener(this);
        }

        registerForContextMenu(posterIV);
    }

    private void loadIntentToView(Intent intent) {
        String movieTitle = intent.getStringExtra(Extras.MOVIE_TITLE);
        String moviePlot = intent.getStringExtra(Extras.MOVIE_PLOT);
        String moviePoster = intent.getStringExtra(Extras.MOVIE_POSTER);
        Float movieRating = intent.getFloatExtra(Extras.MOVIE_RATING, 0f);
        titleET.setText(movieTitle);
        synET.setText(moviePlot);
        Picasso.with(this).load(moviePoster).noFade().into(target);
        ratingBar.setRating(movieRating);
    }

    private void loadDBtoView() {
        Cursor myDBCursor = helper.getRow(movie_id);
        if (myDBCursor.moveToNext()) {
            String name = myDBCursor.getString(myDBCursor.getColumnIndex(MyDB.movieTitle));
            String syn = myDBCursor.getString(myDBCursor.getColumnIndex(MyDB.movieSynopsis));
            myBase64Image = myDBCursor.getString(myDBCursor.getColumnIndex(MyDB.moviePic));
            float rating = myDBCursor.getFloat(myDBCursor.getColumnIndex(MyDB.movieRating));
            int seen = myDBCursor.getInt(myDBCursor.getColumnIndex(MyDB.movieIsWatched));

            titleET.setText(name);
            synET.setText(syn);

            if(seen==1){
                checkBox.setChecked(true);
            }
            if (myBase64Image != null && myBase64Image.length() > 0) {
                Bitmap bitmap = Extras.decodeBase64(myBase64Image);
                posterIV.setImageBitmap(bitmap);
            }
            if (ratingBar != null) {
                ratingBar.setRating(rating);
            }
        }
        myDBCursor.close();
    }

    public void capturePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, Extras.REQUEST_IMAGE_CAPTURE);
        }
    }
// here we get the result image from the take a photo or choose from gallery intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle bundle = data.getExtras();
        if(bundle!= null) {
            if (Extras.REQUEST_IMAGE_CAPTURE == requestCode) {
                tempBitmap = (Bitmap) bundle.get("data");
                if (tempBitmap != null) {
                    checkImageAndScale();
                    posterIV.setImageBitmap(tempBitmap);
                    convertFlag= true;
                }
            }
        }
            if (requestCode == Extras.PICK_IMAGE_REQUEST) {
                if (resultCode == RESULT_OK  && data.getData() != null) {

                    Uri uri = data.getData();
                    try {
                        tempBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        checkImageAndScale();
                        posterIV.setImageBitmap(tempBitmap);
                        convertFlag = true;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

    }
// scale down images that are too big
    private void checkImageAndScale() {
        int height = tempBitmap.getHeight();
        int width = tempBitmap.getWidth();
        if(height>2000 || width > 2000){
            tempBitmap = Bitmap.createScaledBitmap(tempBitmap, 300, 400, false);
        }
    }
// generic info dialog
    private void makeDialog( String msg) {
        LayoutInflater inflater = getLayoutInflater();
        View view=inflater.inflate(R.layout.titlebar, null);
        new AlertDialog.Builder(EditActivity.this)
                .setTitle(Html.fromHtml("<u>Error</u>"))
                .setIcon(android.R.drawable.stat_sys_warning)
                .setMessage(msg)
                .setCancelable(true)
                .setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).create().show();
    }


    // centralized event for all buttons
    @Override
    public void onClick(View v) {
        String imgUrl;
        switch (v.getId()) {
            case R.id.buttonShow:
                imgUrl = imgUrlET.getText().toString();
                if (imgUrl.length() > 0) {
                    Picasso.with(getBaseContext()).load(imgUrl).into(target);
                }
                else{
                    makeDialog("Image url is empty , try a placing a url.");
                }
                break;
            case R.id.buttonOK:

                final ProgressDialog p;
                p = ProgressDialog.show(EditActivity.this, "Loading:", "Loading please wait...", true);
                p.setCancelable(true);
                p.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                p.setMessage("Loading...");
                p.show();

                float rating = ratingBar.getRating();
                // imgUrl = imgUrlET != null ? imgUrlET.getText().toString() : null;
                String name = titleET != null ? titleET.getText().toString() : null;
                String syn = synET != null ? synET.getText().toString() : null;
                int watched = checkBox.isChecked() ? 1:0;
                ContentValues cv = new ContentValues();
                cv.put(MyDB.movieTitle, name);
                cv.put(MyDB.movieSynopsis, syn);
                cv.put(MyDB.movieRating, rating);
                cv.put(MyDB.movieIsWatched, watched);
                if(convertFlag){
                if(tempBitmap!= null) {
                    myBase64Image = Extras.encodeToBase64(tempBitmap);
                }
                }
                if (movie_id == -1 )//add
                {

                    cv.put(MyDB.moviePic, myBase64Image);
                    helper.insert(cv);
                    helper.close();

                    if (isWebContent != -1) {
                        Intent intent = new Intent(EditActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }
                else {
                    if(myBase64Image.length()>0){
                        cv.put(MyDB.moviePic, myBase64Image);
                    }
                    helper.update(movie_id, cv);
                    helper.close();
                }
                finish();
                p.dismiss();
                break;
            case R.id.buttonCancel:
                helper.close();
                finish();
                break;
        }
    }
}
