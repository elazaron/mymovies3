package ml.elazaron.ron.mymovies3;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayOutputStream;

/**
 * Created by ron on 22/07/2016.
 * static data and static methods
 */
public class Extras {
    public static String IS_FROM_WEB = "fromWeb";
    public static String movieID = "movieID";
    public static String MOVIE_TITLE = "title";
    public static String MOVIE_PLOT = "plot";
    public static String MOVIE_POSTER = "poster";
    public static String MOVIE_RATING = "rating";
    public static int resultOnline = 8;

    public static int PICK_IMAGE_REQUEST = 1;
    public static int REQUEST_IMAGE_CAPTURE = 2;

    public static String TRANS_FROM_WEB = "trans_from_web";

    public static String encodeToBase64(Bitmap image) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOS);
        String str = Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
        return str;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }
}
    // not used
//    public static void hideSoftKeyboard(Activity activity) {
//        InputMethodManager inputMethodManager =
//                (InputMethodManager) activity.getSystemService(
//                        Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(
//                activity.getCurrentFocus().getWindowToken(), 0);
//    }
//    Example usage:
//
//    String myBase64Image = encodeToBase64(myBitmap, Bitmap.CompressFormat.JPEG, 100);
//    Bitmap myBitmapAgain = decodeBase64(myBase64Image);

