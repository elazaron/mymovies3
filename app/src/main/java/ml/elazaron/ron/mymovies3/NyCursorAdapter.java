package ml.elazaron.ron.mymovies3;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.R.drawable;
import com.squareup.picasso.Picasso;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;


public class NyCursorAdapter extends CursorAdapter{


    public NyCursorAdapter(Context context, Cursor c) {

        super(context, c);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        return LayoutInflater.from(context).inflate(R.layout.list_item,parent,false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView titleTV = (TextView) view.findViewById(R.id.movieTitleTV);
        TextView synTV = (TextView) view.findViewById(R.id.movieSyn);
        ImageView imgIV= (ImageView) view.findViewById(R.id.movieThumbIV);
        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        CheckBox cb = (CheckBox) view.findViewById(R.id.checkbox1);
//Movie movie = new Movie();
        String title = cursor.getString(cursor.getColumnIndex(MyDB.movieTitle));
        String syn = cursor.getString(cursor.getColumnIndex(MyDB.movieSynopsis));
        String imgUrl = cursor.getString(cursor.getColumnIndex(MyDB.moviePic));
        float rating = cursor.getFloat(cursor.getColumnIndex(MyDB.movieRating));
        int watched  = cursor.getInt(cursor.getColumnIndex(MyDB.movieIsWatched));
        if(watched==1){
            cb.setChecked(true);
        }
        else{
            cb.setChecked(false);
        }
        titleTV.setText(title);
        synTV.setText(syn);
        if( imgUrl!= null && imgUrl.length()>0) {
            Bitmap bitmap = Extras.decodeBase64(imgUrl);
           // Picasso.with(context).load(imgUrl).into(imgIV);
            imgIV.setImageBitmap(bitmap);
        }else{
            imgIV.setImageResource(android.R.drawable.sym_def_app_icon);
        }
        ratingBar.setRating(rating);
      //  mypic.setImageResource(picid);
    }
}
